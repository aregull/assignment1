// All HTML elements that is needed to finish the asked functionality
const getLoanBtn = document.getElementById("loanBtn");
const bankBtn = document.getElementById("bankBtn");
const workBtn = document.getElementById("workBtn");
const repayLoanBtn = document.getElementById("repayLoanBtn");
const buyBtn = document.getElementById("buyBtn");
const bankBalanceEl = document.getElementById("bankBalance");
const payBalanceEl = document.getElementById("payBalance");
const outstandingLoanEl = document.getElementById("outstandingLoan");
const featuresInfo = document.getElementById("features");
const selectLaptop = document.getElementById("selectLaptop");
const laptopInfo = document.getElementById("laptopInfo");
const laptopFeatureTitle = document.getElementById("featureTitle");
const laptopFeaturePhoto = document.getElementById("featurePhotoElement");
const laptopFeaturePrice = document.getElementById("featurePrice")

//////////////////////////////////////////////////////////////////////////////
//                                  BANK                                    //
// Functions and variables that is needed to get the required functionality //
// for the BANK part of the website is placed under here.                   //
//////////////////////////////////////////////////////////////////////////////

let bankBalance = 200;
let numberOfLoans = 0;
let outstandingLoan = 0;

//Alters the inner text of bankBalance, so there will be displayed 
//an amount as soon as the page loads.
bankBalanceEl.innerText = "Balance: " + bankBalance;


/**
 * applyForLoan is triggered when you press the Get a Loan-button and
 * opens a prompt for the user to write their desired amount.
 * If the amount passes all the if-tests then it is within the criteria for
 * getting a loan. This then updates the bankBalance and sets numberOfLoans 
 * to 1 and puts the loaned amount in the outstandingLoan variable. 
 * Then the hideOrShowOutstandingLoan and setPayAndBankBalanceElement-functions
 * are triggered to update the HTML and display for the user. 
 */
function applyForLoan() {
    let loanPrompt = prompt("Please enter loan amount", "");
    
    if (+loanPrompt > (bankBalance*2)){
        alert("You cannot get a loan more than double of your bank balance.")
    }
    else if(numberOfLoans === 1) {
        alert("You cannot get more than one bank loan before buying a computer or repaying your loan!")
    }
    else if (+loanPrompt <= 0) {
        alert("You can't loan zero or a negative amount")
    }
    else {
        bankBalance += +loanPrompt;
        numberOfLoans = 1;
        outstandingLoan = +loanPrompt;
    }
    hideOrShowOutstandingLoan();
    setPayAndBankBalanceElement();
}

/**
 * Function used to decide if the HTML-elements that show outstandingLoan and 
 * the repay-button will be visible or not for the user. Checks if the user
 * currently has a loan, if so, they will be visible. If not, they will be hidden. 
 */
function hideOrShowOutstandingLoan() {
    if (numberOfLoans === 1) {
        outstandingLoanEl.innerText = "Outstanding loan: " + outstandingLoan;
        outstandingLoanEl.style.display = "block";
        repayLoanBtn.style.display = "block";
    } else {
        outstandingLoanEl.style.display = "none";
        repayLoanBtn.style.display = "none";
    }
}

// EVENTLISTENERS FOR BANK SEGMENT
getLoanBtn.addEventListener("click", applyForLoan);

//////////////////////////////////////////////////////////////////////////////
//                                  WORK                                    //
// Functions and variables that is needed to get the required functionality //
// for the WORK part of the website is placed under here.                   //
//////////////////////////////////////////////////////////////////////////////

//Variables needed for Work. Also sets the innerText to show "Pay: 0"
let payAmount = 0;
payBalanceEl.innerText = "Pay: 0";

/**
 * transferPayToBank is triggered when you press the "Bank"-button.
 * The function checks if you have a loan or not, this decides what
 * fucntion to call next. If you have a loan, the deductSalary-function
 * will be called. If you do not have a loan, all the Pay you currently
 * have will be added to the bank balance. Finally, the 
 * setPayAndBankBalanceElement-function is called to update innerTexts.
 */
function transferPayToBank() {
    if (outstandingLoan > 0) {
        deductSalary();
    } else {
        bankBalance += payAmount;
        payAmount = 0;
    }
    setPayAndBankBalanceElement();
} 

/**
 * deductSalary calculates how much to transfer to your loan and 
 * how much to transfer to you bank. If the amount that will
 * go to the outstandingLoan is greater than what you currently
 * owe the bank, the remaining is moved to you bank balance. 
 * Finally, setPayAndBankBalanceElement is called to update 
 * the innerTexts
 */
function deductSalary() {
    let deductedSalary;
    let transferToLoan;
    deductedSalary = payAmount * 0.9;
    transferToLoan = payAmount * 0.1;

    if (transferToLoan >= outstandingLoan) {
        bankBalance += transferToLoan - outstandingLoan;
        numberOfLoans = 0;
        outstandingLoan = 0;
    } else {
        outstandingLoan -= transferToLoan;
    }

    bankBalance = bankBalance + deductedSalary;
    payAmount = 0;
    setPayAndBankBalanceElement(); 
}

/**
 * increasePay increases Pay when the "Pay"-button is pressed.
 * Finally, setPayAndBankBalanceElement is called to update innerTexts.
 */
function increasePay() {
    payAmount += 100;
    setPayAndBankBalanceElement();
}

/**
 * repayLoan is triggered when the "Repay"-button is pressed.
 * The full amount in your pay-balance will go towards the
 * outstandingLoan. If the amount is greater than what you owe
 * then bank, the remainer will be left in your pay-balance.
 * Finally, setPayAndBankBalanceElement is called to update innertexts.
 */
function repayLoan() {
    
    let remainingLoanAmount;
    let remainingPayAmount;
    
    remainingLoanAmount = outstandingLoan - payAmount;
    remainingPayAmount = payAmount - outstandingLoan;

    if(remainingLoanAmount <= 0) {
        outstandingLoan = 0;
        numberOfLoans = 0;
        payAmount = remainingPayAmount;  
    } else {
        outstandingLoan = remainingLoanAmount;
        payAmount = 0;
    }

    setPayAndBankBalanceElement();
}

/**
 * This function updates the innerText of the pay-balance and bank-balance
 */
function setPayAndBankBalanceElement() {
    payBalanceEl.innerText = "Pay: " + payAmount;
    bankBalanceEl.innerText = "Balance: " + bankBalance;
    hideOrShowOutstandingLoan();
}

// EVENT LISTENERS FOR THE WORK SEGMENT
bankBtn.addEventListener("click", transferPayToBank);
workBtn.addEventListener("click", increasePay);
repayLoanBtn.addEventListener("click", repayLoan);

//////////////////////////////////////////////////////////////////////////////
//                                  LAPTOPS                                 //
// Functions and variables that is needed to get the required functionality //
// for the LAPTOP part of the website is placed under here.                 //
// Some of the functionality is taken from the Assignment 1 videos.         //
//////////////////////////////////////////////////////////////////////////////

// Variables needed are declared here
let laptops = [];
let laptopSpecs = [];
let selectedLaptop;

// Fetches the laptops to be displayed in the Komputer Store™
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToMenu(laptops));

/**
 * addLaptopsToMenu loops over all the fetched laptops and further calls
 * the addLaptopToMenu function. It will also set the first laptop to be 
 * visible when the Komputer Store™ loads.
 * @param {*} laptops is all the laptops fetched from the herokuapp-api.
 */
const addLaptopsToMenu = (laptops) => {
    laptops.forEach(x => addLaptopToMenu(x));
    selectedLaptop = laptops[0];
    laptopFeatureTitle.innerText = laptops[0].title;
    laptopFeaturePrice.innerText = laptops[0].price + " kr";
    laptopFeaturePhoto.src = laptops[0].image;
    laptopInfo.innerText = laptops[0].description;
    setLaptopSpecs(laptops[0].specs);
}

/**
 * addLaptopToMenu creates an option-elements with the laptop ID and title
 * which is appended to the select-element showing each laptop.
 * @param {*} laptop is a singluar laptop object from the api
 */
const addLaptopToMenu = (laptop) => {
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    selectLaptop.appendChild(laptopElement);
}

/**
 * handleLaptopMenuChange is resposible for updating the innerText for all
 * elements that is in some way showing information about the chosen laptop.
 * It also calls setLaptopSpecs.
 * @param {*} e is the event that holds information about the currently selected index
 */
const handleLaptopMenuChange = e => {
    selectedLaptop = laptops[e.target.selectedIndex];
    laptopSpecs = selectedLaptop.specs;

    featuresInfo.innerText = "";
    featuresInfo.innerHTML = "";

    laptopInfo.innerText = selectedLaptop.description;
    laptopFeatureTitle.innerText = selectedLaptop.title;
    laptopFeaturePhoto.src = selectedLaptop.image;
    laptopFeaturePrice.innerText = selectedLaptop.price + " kr";
    setLaptopSpecs(selectedLaptop.specs)
}

/**
 * setLaptopSpecs loops through a given laptops specs and displays it on the Komputer Store™
 * @param {*} specs is an array of specs for the chosen laptop from handleLaptopMenuChange
 */
function setLaptopSpecs(specs) {
    for (let i = 0; i < specs.length; i++) {
        const element = specs[i];
        featuresInfo.innerText += element;
        featuresInfo.innerHTML += "<br/>"
    }
}

/**
 * buySelectedLaptop checks if you currently have enough money to buy the selected laptop.
 * If you have enough, the price of the laptop is removed from you bank-balance, it displays
 * a message which states what laptop you bought and updates the innertext of your bank balance
 * through setPayAndBankBalanceElement. If you don't have enough money, you will be alerted that
 * you need to work harder.
 */
function buySelectedLaptop() {
    if (bankBalance < selectedLaptop.price) {
        alert("INSUFFICIENT FUNDS. Work harder")
    } else {
        bankBalance -= selectedLaptop.price
        alert("Congratulations, you are now the proud owner of the " 
        + selectedLaptop.title + "!")
        setPayAndBankBalanceElement();
    }
}


// EVENT LISTENERS FOR THE LAPTOP SEGMENT
buyBtn.addEventListener("click", buySelectedLaptop);
selectLaptop.addEventListener("change", handleLaptopMenuChange);